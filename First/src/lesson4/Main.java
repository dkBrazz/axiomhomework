package lesson4;

import java.util.Scanner;

public class Main {

    public static void getResult(GeomFigura shape, String method){
        if (method.equals("Square")) {
            System.out.println("Square " + shape.getName() + " is " + shape .getArea());
        } else if (method.equals("Perimeter")) {
            System.out.println("Square " + shape.getName() + " is " + shape.getPerimeter());
        }
    }

    public static void main(String[] args) {
        GeomFigura[] arrFigure = {
                new Circle(),
                    new Triangle(),
                    new Square(),
                    new Rectangle(),
                    new Trapezium()
        };

        for (int i = 0; i < arrFigure.length; i++) {
           System.out.println(arrFigure[i].getName());
        }
        System.out.println("Enter Figure: ");
        Scanner scanFig = new Scanner(System.in);
        String menuItem = scanFig.nextLine();

        System.out.println("Square or Perimeter?");
        Scanner scanFigVal = new Scanner(System.in);
        String value = scanFigVal.nextLine();

        if(menuItem.equals(arrFigure[0].getName())) {
            System.out.println("Enter radius:");
            Scanner scanCircle = new Scanner(System.in);
            int circleRadius = scanCircle.nextInt();

            Circle myCircle = new Circle(circleRadius);
            getResult(myCircle, value);
        }
        else if (menuItem.equals(arrFigure[1].getName())) {
            System.out.println("Enter sideA:");
            Scanner scanTriangleA = new Scanner(System.in);
            int triangleSideA = scanTriangleA.nextInt();
            System.out.println("Enter sideB:");
            Scanner scanTriangleB = new Scanner(System.in);
            int triangleSideB = scanTriangleB.nextInt();
            System.out.println("Enter sideC:");
            Scanner scanTriangleC = new Scanner(System.in);
            int triangleSideC = scanTriangleC.nextInt();

            Triangle myTriangle = new Triangle(triangleSideA, triangleSideB, triangleSideC);
            getResult(myTriangle, value);
        }
        else if (menuItem.equals(arrFigure[2].getName())) {
            System.out.println("Enter sideA:");
            Scanner scanSquare = new Scanner(System.in);
            int squareSideA = scanSquare.nextInt();

            Square mySquare = new Square(squareSideA);
            getResult(mySquare, value);
        }
        else if (menuItem.equals(arrFigure[3].getName())) {
            System.out.println("Enter sideA:");
            Scanner scanRectangleA = new Scanner(System.in);
            int rectangleSideA = scanRectangleA.nextInt();
            System.out.println("Enter sideB:");
            Scanner scanRectangleB = new Scanner(System.in);
            int rectangleSideB = scanRectangleB.nextInt();

            Rectangle myRectangle = new Rectangle(rectangleSideA, rectangleSideB);
            getResult(myRectangle, value);
        }
        else if (menuItem.equals(arrFigure[4].getName())) {
            System.out.println("Enter sideA:");
            Scanner scanTrapeziumA = new Scanner(System.in);
            int trapeziumSideA = scanTrapeziumA.nextInt();
            System.out.println("Enter sideB:");
            Scanner scanTrapeziumB = new Scanner(System.in);
            int trapeziumSideB = scanTrapeziumB.nextInt();
            System.out.println("Enter sideC:");
            Scanner scanTrapeziumHeight = new Scanner(System.in);
            int trapeziumHeight = scanTrapeziumHeight.nextInt();

            Trapezium myTrapezium = new Trapezium(trapeziumSideA, trapeziumSideB, trapeziumHeight);
            getResult(myTrapezium, value);
        }
    }

}

