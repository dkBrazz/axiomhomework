package lesson4;

import static java.lang.Math.PI;
import static java.lang.Math.*;

public class Circle extends GeomFigura{
    private static final String name = "Circle";
    private float radius;

    public Circle(float radius) {
        if(radius>0)
            this.radius = radius;
        else{
            System.out.println("Radius not allowed 0 or less. ");
        }
    }
    public Circle() {}

    @Override
    public double getArea() {
        return PI*pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return PI;
    }

    @Override
    public String getName() {
        return name;
    }
}
