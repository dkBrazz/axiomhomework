package lesson4;

import static java.lang.Math.*;

public class Triangle extends GeomFigura {
    private static final String name = "Triangle";
    private float sideA, sideB, sideC;
    private double midlOfPerimeter;

    public Triangle(float sideA, float sideB, float sideC) {
        if(sideA > 0 && sideB > 0 && sideC > 0) {
            this.sideA = sideA;
            this.sideB = sideB;
            this.sideC = sideC;
            midlOfPerimeter = (sideA + sideB + sideC) / 2;
        }
        else {
            System.out.println("All value must be more 0.");
        }
    }
    public Triangle() {}
    @Override
    public double getArea() {
        return sqrt(midlOfPerimeter*(midlOfPerimeter-sideA)*(midlOfPerimeter-sideB)*(midlOfPerimeter-sideC));
    }

    public double getPerimeter(){
        return midlOfPerimeter * 2;
    }

    @Override
    public String getName() {
        return name;
    }
}
