package lesson4;

public class Rectangle extends GeomFigura {
    private static final String name = "Rectangle";
    private float sideA, sideB;


    public Rectangle(float sideA, float sideB) {
        if(sideA > 0 && sideB > 0) {
            this.sideA = sideA;
            this.sideB = sideB;
        }
        else{
            System.out.println("All value must be more 0.");
        }
    }
    public Rectangle() {}
    @Override
    public double getArea() {
        return sideA*sideB;
    }

    @Override
    public double getPerimeter() {
        return 2*sideA + 2*sideB;
    }

    @Override
    public String getName() {
        return name;
    }
}
