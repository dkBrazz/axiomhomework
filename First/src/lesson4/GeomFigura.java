package lesson4;

public abstract class GeomFigura {
    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract String getName();
}
