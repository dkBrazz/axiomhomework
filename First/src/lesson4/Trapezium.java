package lesson4;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Trapezium extends GeomFigura {

    private static final String name = "Trapezium";
    private float sideA, sideB, height;

    public Trapezium(float sideA, float sideB, float height) {
        if(sideA > 0 && sideB > 0 && height > 0){
            this.sideA = sideA;
            this.sideB = sideB;
            this.height = height;
        }
        else
        {
            System.out.println("All values must be more 0.");
        }

    }
    public Trapezium() {}
    @Override
    public double getArea() {
        return (double) (((sideA+sideB)*height) / 2);
    }

    @Override
    public double getPerimeter() {
        double sideCD = sqrt(pow((sideB - sideA)/2, 2) + pow(height, 2));
        return sideA + sideB + sideCD*2;
    }

    @Override
    public String getName() {
        return name;
    }
}
