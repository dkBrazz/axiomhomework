package lesson4;

public class Square extends GeomFigura{
    private static final String name = "Square";
    private float sideA;


    public Square(float sideA) {
        if(sideA>0)
            this.sideA = sideA;
        else{
            System.out.println("Side of square must be more 0.");
        }

    }
    public Square() {}
    @Override
    public double getArea() {
        return sideA*4;
    }

    @Override
    public double getPerimeter() {
        return sideA *4;
    }

    @Override
    public String getName() {
        return name;
    }
}
